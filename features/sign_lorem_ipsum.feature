Feature: Sign the Lorem Ipsum
  As a user
  In order to approve the content of the Lorem Ipsum
  I want to sign it

Background:
  Given I am on the Lorem Ipsum page

Scenario: using a valid signature
  When I submit the form with "Bobby" as signature
  Then I should see the text "Bobby"

Scenario: using a too long signature
  When I submit the form with "Neque porro quisquam est qui dolorem ipsum qui" as signature
  Then I should see "The signature can't be longer than 40 characters"
