Rails.application.routes.draw do
  root 'signatures#index'
  get 'pages/index'
  get 'pages/lorem_ipsum'
  get 'hello_world/index'
  resources :signatures, only: [:index, :new, :create]
  get 'lorem_ipsum' => 'signatures#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
