require 'rails_helper'

RSpec.describe PagesController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #lorem_ipsum" do
    it "returns http success" do
      get :lorem_ipsum
      expect(response).to have_http_status(:success)
    end
  end

end
